# Get-AppxPackage | Select Name, PackageFullName

$appxlist = New-Object System.Collections.ArrayList

foreach ($appx in (Get-AppxPackage *ZuneMusic*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *ZuneVideo*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *BingNews*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *BingSports*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *BingFinance*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *BingWeather*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *MarchofEmpires*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *DisneyMagicKingdoms*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *BubbleWitch3Saga*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *CandyCrush*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *ParadiseBay*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *Minecraft*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *MicrosoftSolitaireCollection*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *Facebook*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *Microsoft.Getstarted*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *MicrosoftOfficeHub*)) { $appxlist.Add($appx) > $null }
#foreach ($appx in (Get-AppxPackage *StorePurchaseApp*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *WindowsFeedbackHub*)) { $appxlist.Add($appx) > $null }
foreach ($appx in (Get-AppxPackage *FitbitCoach*)) { $appxlist.Add($appx) > $null }
#foreach ($appx in (Get-AppxPackage *xbox* | Where-Object {$_.Name -ne "Microsoft.XboxGameCallableUI"} )) { $appxlist.Add($appx) > $null }
#Cortana
foreach ($appx in (Get-AppxPackage -allusers Microsoft.549981C3F5F10)) { $appxlist.Add($appx) > $null }

#nogo used by Mail :/ foreach ($appx in (Get-AppxPackage *Microsoft.People*)) { $appxlist.Add($appx) > $null }
#nogo so you cant remove a preview :( foreach ($appx in (Get-AppxPackage *CBSPreview*)) { $appxlist.Add($appx) > $null }
#nogo man i actually payed for my windows and i still get this shit :( foreach ($appx in (Get-AppxPackage *Microsoft.Advertising.Xaml*)) { $appxlist.Add($appx) > $null }

Write-Host "Found" $appxlist.Count "AppxPackages to remove"
#foreach ($appx in $appxlist) { Write-Host $appx.Name }

foreach ($appx in $appxlist) {
    Write-Host "Removing" $appx.Name
    Remove-AppxPackage $appx
}

& ".\AppxPackageReport.ps1"
