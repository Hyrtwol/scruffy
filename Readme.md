# Windows Cleaner

Remove the bubblegum from windows.

![](Artwork/Scruffy.png)

## PowerShell Setup

```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
```

## Research

* https://www.ghacks.net/2017/04/07/windows-10-creators-update-apps-uninstall/
* https://www.windowscentral.com/how-uninstall-cortana-windows-10-may-2020-update

Remove Cortana:
```
Get-AppxPackage -allusers Microsoft.549981C3F5F10 | Remove-AppxPackage
```
